CONTENTS OF THIS FILE
---------------------
* Introduction
* Requirements
* Installation
* Configuration
* How It Works
* Troubleshooting
* Maintainers

INTRODUCTION
------------
This module provides a Drupal Commerce payment method to embed the payment
services provided by Adyen. It efficiently integrates payments from various
sources such as credit cards, bank transfers, PayPal and or also mobile phones.
This modules only supports the Adyen Hosted Payment Pages (HPP) solution and
not direct API integration. The module also supports the
Adyen manual capture process.

REQUIREMENTS
------------
This module requires the following:
* Submodules of Drupal Commerce package (https://drupal.org/project/commerce)
  - Commerce core
  - Commerce Payment (and its dependencies)
* Adyen php api library
* Adyen Merchant account (https://www.adyen.com/signup)

INSTALLATION
------------
* This module needs to be installed via Composer, which will download
the required libraries.
composer require "drupal/commerce_adyen"
https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies

CONFIGURATION
-------------
* Create a new Adyen payment gateway.
  Administration > Commerce > Configuration >
  Payment gateways > Add payment gateway
  It is recommended to enter test credentials and then override these with live
  credentials in settings.php. This way, live credentials will not be stored in
  the db.

HOW IT WORKS
------------
* General considerations:
  - The store owner must have a Adyen merchant account.
    Sign up here:
    https://www.adyen.com/signup
  - https://docs.adyen.com/classic-integration/hosted-payment-pages
    documentation

TROUBLESHOOTING
---------------
* No troubleshooting pending for now.

Note about payment methods
--------------------------
Make sure that you have added payment methods to your merchant account. If you
haven't done so already, add them in your Customer Area.

* Log in to your Customer Area.
* Switch to your merchant account.
* Go to Account > Payment methods.
* Select Add payment methods.
* Start entering the name of the payment method, then select it from the
  drop-down list.
* Select Submit.
